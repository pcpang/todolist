/**
 * Created by pohchye on 23/7/2016.
 */

// Initialize Express
var express = require("express");
var app = express();

// Initialize Body-Parser to parse body parameters for POST method
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({"extended": "true"}));
app.use(bodyParser.json());

// Use promise on Express
// var queue = require("q");
var q = require("q");

// Initialize MySql connection
var mysql = require("mysql");
// Create mysql connection pool to database
var pool = mysql.createConnection({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "todolist",
    connectionLimit: 4
});

// --- Functions ---

// Display array
function displayArray(Objs) {
    // console.info("Number of objects:", Objs.length);
    var arr_obj = {};
    var prop = "";
    for (var array_index in Objs) {
        if (Objs.hasOwnProperty(array_index)) {
            console.info(">> Index:", array_index);
            arr_obj = Objs[array_index];
            for (prop in arr_obj) {
                if (arr_obj.hasOwnProperty(prop)) {
                    console.info("> " + prop + " = " + arr_obj[prop]);
                }
            }
        }
    }
}

// SQL make query function
var makeQuery = function (sql, pool) {
    return function (args) {

        var defer = q.defer();
        pool.getConnection(function (err, connection) {
            if (err) {
                return defer.reject(err);
            }
            connection.query(sql, args || [], function (err, result) {
                connection.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(result);
            });
        });
        return defer.promise;
    }
};

// --- End of Functions ---

// --- Create SQL query functions ---

// Sample create query function
const SQL_STATEMENT = "? ?";
var make_query_function = makeQuery(SQL_STATEMENT, pool);
var args = "[" + "ABC, DEF" + "]"; //sample args
// // Executing the query
// make_query_function(args)
//     .then(function (result) {
//         console.log(result);
//         // successCallback(result);
//         // res.status(200).json(result);
//     })
//     .catch(function (err) {
//         console.log(err);
//         // errorCallback(err);
//         // res.status(500).end();
//     });

// --- End of Create SQL query functions ---

// --- Initialize Variables ---

// Test Variables
var data_array = [];
var data_object = {};

// --- End of Initialize Variables ---

// --- Class ---

// Test class
var test_class = function (arg1, arg2) {
    this.arg1 = arg1;
    this.arg2 = arg2;
};
// Test class method
test_class.prototype.test_method = function (successCallback, errorCallback) {
    console.log("- Executing method (test_method) for class (test_class)");
    var self = this;
    // Test variables
    var err = "";
    var result = {
        arg1: self.arg1,
        arg2: self.arg2
    };
    // Execution of a function call like sql query should execute the following to check
    if (err) {
        return errorCallback(err);
    }
    successCallback(result);
};

// --- End of Class ---

// --- Middleware ----

// Sample Post
app.post("/api/post/test", function (req, res) {  //pass in next if need to use next()
    console.log("* Executing API (/api/post/test)");
    // Retrieve parameters from request
    console.log("> Post Received Param1", req.body.params1);
    console.log("> Post Received Param2", req.body.params2);
    // Push retrieve parameters to array
    console.log("> Push params to array");
    data_array.push(req.body.params1);
    data_array.push(req.body.params2);
    displayArray(data_array);
    // Push retrieve parameters to object
    console.log("> Push params to object");
    data_object["user1"] = [];
    data_object["user1"].push(req.body.params1);
    data_object["user1"].push(req.body.params1);
    data_object["user2"] = [];
    data_object["user2"].push(req.body.params2);
    data_object["user2"].push(req.body.params2);
    displayArray(data_object);
    // Test Variables to send response back
    var params = {
        params1: {EPost1_1: "posting1_1", EPost1_2: "posting1_2"},
        params2: {EPost2_1: "posting2_1", EPost2_2: "posting2_2"}
    };
    // Creating object for class test_class
    var newObj = new test_class(params.params1, params.params2);
    // Execute method test_method() for newObj
    newObj.test_method(
        // successCallback(result)
        function success(result) {
            // Return response back to service
            console.log("> Post Sent Param1:", result.arg1);
            console.log("> Post Sent Param2:", result.arg2);
            res.status(202).json(result);
            // res.status(202).end();
    },
        // errorCallback(err)
        function error(err) {
            console.log("Error occurred:", err);
            res.status(500).end();
        }
    );
});

// Sample Get - Parameters obtain from header
app.get("/api/get/test", function (req, res) {  //pass in next if need to use next()
    console.log("* Executing API (/api/get/test)");
    // Retrieve parameters from request
    console.log("> Post Received Param1: " + req.query.params1);
    console.log("> Post Received Param2: " + req.query.params2);
    // Test Variables to send response back
    var params = {
        params1: {EPost1_1: "posting1_1", EPost1_2: "posting1_2"},
        params2: {EPost2_1: "posting2_1", EPost2_2: "posting2_2"}
    };
    
    // Creating object for class test_class
    var newObj = new test_class(params.params1, params.params2);
    // Execute method test_method() for newObj
    newObj.test_method(
        // successCallback(result)
        function success(result) {
            // Return response back to service
            console.log("> Post Sent Param1", result.arg1);
            console.log("> Post Sent Param2", result.arg2);
            res.status(202).json(result);
            // res.status(202).end();
        },
        // errorCallback(err)
        function error(err) {
            console.log("Error occurred:", err);
            res.status(500).end();
        }
    );
});

// Sample Get - Parameters obtain from URL
app.get("/api/get/urlParameter/:url_param", function (req, res) {  //pass in next if need to use next()
    console.log("* Executing API (/api/get/urlParameter)");
    // Retrieve parameters from request
    // console.log("> Post Received Param1:", req.query.Post1);
    console.log("> Post Received URL Param: " + req.params.url_param);
    // Test Variables to send response back
    var params = {
        params1: {EPost1_1: "posting1_1", EPost1_2: "posting1_2"},
        params2: {EPost2_1: "posting2_1", EPost2_2: "posting2_2"}
    };
    // Creating object for class test_class
    var newObj = new test_class(params.params1, params.params2);
    // Execute method test_method() for newObj
    newObj.test_method(
        // successCallback(result)
        function success(result) {
            // Return response back to service
            console.log("> Post Sent Param1", result.arg1);
            console.log("> Post Sent Param2", result.arg2);
            res.status(202).json(result);
            // res.status(202).end();
        },
        // errorCallback(err)
        function error(err) {
            console.log("Error occurred:", err);
            res.status(500).end();
        }
    );
});

// Serving public files
app.use(express.static(__dirname + "/public"));
// Serving bower modules
app.use("/bower_components", express.static(__dirname + "/bower_components"));
// Handle anything that does not match
app.use(function (req, res) {
    res.redirect("error.html");
});

// --- End of Middleware ----

// Set the port
// app.set("port", process.argv[2] || process.env.ARGV_PORT || 3001);
app.set("port", 3001);

// Start the express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port", app.get("port"));
});