/**
 * Created by pohchye on 24/7/2016.
 */

(function () {
    angular.module("ToDoApp")
        .service("ToDoSvc", ToDoSvc);
    
    // Inject service with http and q
    ToDoSvc.$inject = ["$http", "$q"];
    
    function ToDoSvc($http, $q) {
        console.log("Executing Service ToDoSvc");
    
        // Create the service handler
        var service = this;
    
        // --- Initialize Variables ---
        // Test variables
        var data_params = {
            params1: {APost1_1: "posting1_1", APost1_2: "posting1_2"},
            params2: {APost2_1: "posting2_1", APost2_2: "posting2_2"}
        };
        var data_param = "URL_Parameter";
    
        // --- Define the actions ----
    
        // Test Service action1
        service.post_action1 = function () {
            console.log("* Executing ToDoSvc function: post_action1");
            // Create the promise handler
            var defer = $q.defer();
            $http.post("/api/post/test", data_params)
            .then(function (result) {
                console.log("> Service Result:", result.data);
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Service Error:", err);
                return defer.reject(err);
            });
            return defer.promise;
        };

        // Test Service action2
        service.get_action2 = function () {
            console.log("* Executing ToDoSvc function: get_action2");
            // Create the promise handler
            var defer = $q.defer();
            // Note: Cannot use post method to pass the parameters, require the params keyword
            $http.get("/api/get/test", { params: {
                    params1: {APost1_1: "posting1_1", APost1_2: "posting1_2"},
                    params2: {APost2_1: "posting2_1", APost2_2: "posting2_2"}
                }}
            ).then(function (result) {
                console.log("> Service Result:", result.data);
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Service Error:", err);
                return defer.reject(err);
            });
            return defer.promise;
        };

        // Test Service action3 - using parameters for get
        service.get_action3 = function () {
            console.log("* Executing ToDoSvc function: get_action3");
            // Create the promise handler
            var defer = $q.defer();
            // Note: Cannot use post method to pass the parameters, require the params keyword
            $http.get("/api/get/urlParameter/" + data_param
            ).then(function (result) {
                console.log("> Service Result:", result.data);
                return defer.resolve(result.data);
            }).catch(function (err) {
                console.log("> Service Error:", err);
                return defer.reject(err);
            });
            return defer.promise;
        }
    }

})();