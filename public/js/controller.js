/**
 * Created by pohchye on 23/7/2016.
 */

function displayObj(obj) {
    console.info("Type for obj:", typeof obj);
    for (var prop in obj) {
        if (prop == "__proto__") continue;
        if (obj.hasOwnProperty(prop)) {
            console.info("> " + prop + " = " + obj[prop]);
        }
    }
}

function displayArray(Objs) {
    // console.info("Number of objects:", Objs.length);
    var arr_obj = {};
    var prop = "";
    for (var array_index in Objs) {
        if (Objs.hasOwnProperty(array_index)) {
            console.info(">> Index:", array_index);
            arr_obj = Objs[array_index];
            for (prop in arr_obj) {
                if (arr_obj.hasOwnProperty(prop)) {
                    console.info("> " + prop + " = " + arr_obj[prop]);
                }
            }
        }
    }
}

(function () {
    angular.module("ToDoApp")
        .controller("ToDoCtrl", ToDoCtrl);
    
    function ToDoCtrl(ToDoSvc) {
        console.log("Executing Controller ToDoCtrl");
    
        // Create the controller handler
        var vm = this;
    
        // Initialize Variables - Model (data bind variables to View HTML)
        vm.receiveItems = [];
        vm.status = {
            message: "",
            code: 0
        };
    
        // --- Define the actions - Model (data bind actions to View HTML) ----
    
        // Controller action1
        vm.post_action1 = function () {
            console.log("* Executing ToDoCtrl function: post_action1");
            ToDoSvc.post_action1(vm.formItem)
                .then(function (result) {
                    console.log("> Controller Result:", result);
                    // console.log(">", displayObj(result));
                    console.log(">", displayArray(result));
                    vm.receiveItems = result;
                    vm.message = "post_action1: success";
                })
                .catch(function (err) {
                    console.log("> Controller Error:", err);
                    vm.message = "post_action1: error";
                });
        };

        // Controller action2
        vm.get_action2 = function () {
            console.log("* Executing ToDoCtrl function: get_action2");
            ToDoSvc.get_action2(vm.formItem)
                .then(function (result) {
                    console.log("> Controller Result:", result);
                    // console.log(">", displayObj(result));
                    console.log(">", displayArray(result));
                    vm.message = "post_action2: success";
                })
                .catch(function (err) {
                    console.log("> Controller Error:", err);
                    vm.message = "post_action2: error";
                });
        };

        // Controller action3
        vm.get_action3 = function () {
            console.log("* Executing ToDoCtrl function: get_action3");
            ToDoSvc.get_action3(vm.formItem)
                .then(function (result) {
                    console.log("> Controller Result:", result);
                    // console.log(">", displayObj(result));
                    console.log(">", displayArray(result));
                    vm.message = "post_action3: success";
                })
                .catch(function (err) {
                    console.log("> Controller Error:", err);
                    vm.message = "post_action3: error";
                });
        }
    }
    
    // Inject controller with service
    ToDoCtrl.$inject = ["ToDoSvc"];
})();